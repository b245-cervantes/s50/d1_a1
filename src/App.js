import AppNavBar from './components/AppNavBar';
import './App.css';
import Home from './pages/Home';

import { useState, useEffect } from 'react';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
// import modules from react-router-dom for the routing
import { BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import Logout from './pages/Logout';
import PageNotFound from './pages/PageNotFound';
import { UserProvider } from './UserContext';
import CourseView from './pages/CourseView';


function App() {
  const [user, setUser] = useState(null);
  
  
  const unSetUser = () => {
    localStorage.clear();
    
  }

  useEffect(() => {
      fetch(`${process.env.REACT_APP_API_URL}/user/details`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
      .then(result => result.json())
      .then(data => {
        if(localStorage.getItem('token')!==null){
          setUser({
            id:data._id,
            isAdmin: data.isAdmin
          })
        }else{
          setUser(null)
        }
      })
  }, [])
  //Storing information in a context object is don by providing information using the corresponding "provider" and passing
  //information thru the prop value;
  //all information/data provided to the provider
  return (
    <UserProvider value = {{user, setUser, unSetUser}}>
       <Router>
       <AppNavBar />
       <Routes>
         <Route path='/' element ={<Home />} />
         <Route path='/courses' element ={<Courses />} />
         <Route path='/register' element ={<Register />} />
         <Route path='/login' element ={<Login />} />
         <Route path='/logout' element={<Logout />} />
         <Route path='*' element={<PageNotFound />}/>
         <Route path='/course/:courseId' element = {<CourseView />} />
       </Routes>
       
     </Router>
     
    </UserProvider>
   
  );
}

export default App;
