import React from 'react';
import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


function Home() {
  return (
        <Fragment>
            <Banner />
             <Highlights />
            
        </Fragment>
  )
}

export default Home