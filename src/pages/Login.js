import React, { Fragment } from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import { useState,useEffect, useContext } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


function Login() {



        const [email, setEmail] = useState("");
        const [password, setPassword] = useState("");
        const [isActive, setIsActive] =useState(false)

        // const [user, setUser] = useState(localStorage.getItem('email'))
            const navigate = useNavigate();
        const {user,setUser} = useContext(UserContext);
        console.log(user)

        useEffect(()=>{
                console.log(email)
                console.log(password)
        },[email, password])

        useEffect(()=>{
            if(email !=="" && password !==""){
                setIsActive(true)
            } else {
                setIsActive(false)
            }
        })
         
        const login = (event) => {
            event.preventDefault()
            //if you want to add the email of the authenticatd user in local storage
            


            // Process a fecth request to corresponding backend API
            //syntax: fetch('url', {options})


                fetch(`${process.env.REACT_APP_API_URL}/user/login`,{
                    method: 'POST',
                    headers : {
                        'Content-Type' : 'application/json'
                    },
                    body: JSON.stringify({
                        email:email,
                        password:password
                    })
                }).then(result => result.json())
                .then(data => {
                    console.log(data)

                    if(data ===false){
                        Swal.fire({
                            title: 'Authentication Failed!!!',
                            icon: 'error',
                            text: 'Please Try Again!'
                           
                        })
                    } else {
                        localStorage.setItem('token', data.auth);
                        retrieveUserDetails(localStorage.getItem('token'))
                        Swal.fire({
                            title: 'Authentication Successfull!',
                            icon: 'success',
                            text: 'Welcome to Zuitt'
                           
                        })
                        navigate('/');
                    }
                })

            // localStorage.setItem('email', email);
            // setUser(localStorage.getItem('email'))
            // alert("You are now Logged in!")
            // setEmail('');
            // setPassword('');
            // navigate('/')
        }
        const retrieveUserDetails = (token) => {
            // the token sent as part of the request's hearder information
            
            fetch(`${process.env.REACT_APP_API_URL}/user/details`, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
            .then(result => result.json())
            .then(data => {
                console.log(data)

               setUser({
                id: data._id,
                isAdmin: data.isAdmin
               })
            })
        }
        
  return (

     user ?
     <Navigate to = '/*' />
        :
   <Fragment>
            <h1 className='text-center mt-5'>Login</h1>
            <Form className='mt-5 mb-5' onSubmit={event => login(event)}>
            <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control type="email" placeholder="Enter email" 
                value={email} 
                onChange={event => setEmail(event.target.value)} required />
            <Form.Text className="text-muted">
                 We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control type="password" placeholder="Password"
                    value={password} 
                    onChange ={event => setPassword(event.target.value)} required/>
            </Form.Group>

            {
                isActive ? 
                <Button variant="success" type="submit" >
                         Login
                 </Button>
                 :
                 <Button variant="danger" type="submit" disabled>
                         Login
                 </Button>
                 
            }
            </Form>
   </Fragment>
  )
}

export default Login





















