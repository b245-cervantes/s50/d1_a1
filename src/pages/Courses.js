import React, { useEffect, useState } from 'react';
import courses from '../database/courses';
import CourseCard from '../components/CourseCard';
import { Fragment } from 'react';


function Courses() {
    // console.log(courses)
    // const course = courses.map(course => {

    //     return (
    //         <CourseCard key={course.id} courses = {course}/>
    //       )
        
    // })

    const [courses, setCourses] = useState([])

    useEffect(() => {
      fetch(`${process.env.REACT_APP_API_URL}/course/allActive`)
      .then(result => result.json())
      .then(data => {
          setCourses(data.map(course => {
            return(<CourseCard key ={course._id} courses = {course}/>)
          }))
      })
    })
 
  return (
            <Fragment>
            <h1 className='text-center mt-3'>Courses</h1>
                 {courses}

            </Fragment>
       
    )
}

export default Courses