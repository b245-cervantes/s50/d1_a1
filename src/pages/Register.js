import React, { Fragment } from 'react'
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
// import the hooks that are needed in our page
import { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

function Register() {
    ///CREATE 3 new states where we will store tje value from input of the email, password and confirmPassword
        const [firstName, setFirstName] = useState("");
        const [lastName, setLastName] = useState("");
        const [email, setEmail] = useState("");
        const [password, setPassword] = useState("");
        const [confirmPassword, setConfirmPassword] = useState("");
        const [mobileNum, setMobileNum] = useState("");

      const [isActive, setIsActive] = useState(false)
      
      const {user, setUser} = useContext(UserContext);
      const navigate = useNavigate();
      

      useEffect(()=>{
          if(email !=="" && password !== "" && confirmPassword !=="" && password === confirmPassword){
            setIsActive(true)
          } else {
            setIsActive(false)
          }
      }, [email, password, confirmPassword])
        
      function register(event){
         event.preventDefault()
          
         fetch(`${process.env.REACT_APP_API_URL}/user/register`, {
          method: 'POST',
          headers: {
            'Content-Type' : 'application/json'
          },
          body: JSON.stringify({
            firstName:firstName,
            lastName:lastName,
            email:email,
            password: password,
            confirmPassword:confirmPassword,
            mobileNo: mobileNum
          })
         })
         .then(result => result.json())
         .then(data => {
           if(data) {
            
           Swal.fire({
              title: 'CONGRATULATIONS!!',
              icon: 'success',
              text: 'Your registration has been successful!'
             
          })
          navigate('/login')
          console.log(data)
           } else {
            
            Swal.fire({
              title: 'Registration Failed!!!',
              icon: 'error',
              text: 'Email is already in used!!'
             
          })

           }
         })

        //  localStorage.setItem('email', email);
        //  setUser(localStorage.getItem('email'))
        //  navigate('/');
        //   alert("Congratulations, you are now registered on our website!!");
        //   setEmail('');
        //   setPassword('');
        //   setConfirmPassword('');
      }
   
  return (
      user ?
      <Navigate to = '/*'/>
      :
    <Fragment>
        <h1 className='text-center mt-5'>Register</h1>
        <Form className='mt-5' onSubmit={event => register(event)}>
        <Form.Group className="mb-3" controlId="formBasicFirstName">
        <Form.Label>Firstname:</Form.Label>
        <Form.Control type="text" placeholder="Enter firstname" value={firstName} 
        onChange = {event => setFirstName (event.target.value)} required/>
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicLastName">
        <Form.Label>Lastname:</Form.Label>
        <Form.Control type="text" placeholder="Enter lastname" value={lastName} 
        onChange = {event => setLastName (event.target.value)} required/>
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control 
                type="email" 
                placeholder="Enter email" 
                value ={email}
                onChange = {event => setEmail (event.target.value)}
                required
                />
        <Form.Text className="text-muted">
            We'll never share your email with anyone else.
        </Form.Text>
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicPassword">
        <Form.Label>Password</Form.Label>
        <Form.Control type="password" placeholder="Password" value={password} 
        onChange = {event => setPassword (event.target.value)} required/>
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicConfirmPassword">
        <Form.Label>Confirm Password</Form.Label>
        <Form.Control 
                type="password" 
                placeholder="Confirm Password" value={confirmPassword} 
                onChange = {event => setConfirmPassword (event.target.value)}
                  required
                />
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicMobile">
        <Form.Label>Mobile number:</Form.Label>
        <Form.Control type="text" placeholder="Enter mobile number" value={mobileNum}
        onChange = {event => setMobileNum (event.target.value)} required/>
        </Form.Group>

        {
          isActive ?
          <Button variant="primary" type="submit">
            Submit
         </Button>
         :
         <Button variant="danger" type="submit" disabled>
            Submit
         </Button>
        }
       
      
    </Form>
    </Fragment>
  )
}

export default Register