import React from 'react'

import {Container , Nav , Navbar} from 'react-bootstrap'
import {Link, NavLink} from 'react-router-dom';
import { useContext, Fragment } from 'react';
import UserContext from '../UserContext';

function AppNavBar() {

  console.log(localStorage.getItem("email"))
  const {user} = useContext(UserContext);
    // const [user, setUser] = useState(localStorage.getItem('email'))
  return (
    <Navbar bg="light" expand="lg">
    <Container>
      <Navbar.Brand as = {Link} to = '/'>Zuitt</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="ms-auto">
          <Nav.Link as = {NavLink} to = '/'>Home</Nav.Link>
          <Nav.Link as = {NavLink} to = '/courses'>Courses</Nav.Link>
          {/* condtional render */}
          {
              user ? 
              <Nav.Link as = {NavLink} to = '/logout' >Logout</Nav.Link>
              :
              <Fragment>
              <Nav.Link as = {NavLink} to = '/register'>Register</Nav.Link>
              <Nav.Link as = {NavLink} to = '/login'>Login?</Nav.Link>
              </Fragment>
          }
        

        </Nav>
      </Navbar.Collapse>
    </Container>
  </Navbar>
  )
}

export default AppNavBar