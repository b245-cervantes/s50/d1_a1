import React from 'react';
import {Col, Row, Card} from 'react-bootstrap'



function Highlights() {
  return (
    
        <Row className='mt-5'>
       
            <Col className='col-md-4 col-10 mx-auto m-md-0 m-1'>
             {/* First card */}
                <Card className='cardHiglight' >
                <Card.Body>
                <Card.Title>Learn from home</Card.Title>
                <Card.Text>
                    Some quick example text to build on the card title and make up the
                    bulk of the card's content.
                </Card.Text>
                </Card.Body>
                </Card>
             </Col>
                 {/* Second card */}
             <Col className='col-md-4 col-10 mx-auto m-md-0 m-1'>    
                <Card className='cardHiglight' >
                <Card.Body>
                <Card.Title>Study Now, Pay Later </Card.Title>
                <Card.Text>
                    Some quick example text to build on the card title and make up the
                    bulk of the card's content.
                </Card.Text>
                </Card.Body>
                </Card>
            </Col>   
                {/* third card */}
            <Col className='col-md-4 col-10 mx-auto'>    
                <Card className='cardHiglight'>
                <Card.Body>
                <Card.Title>Be Part of Our Community</Card.Title>
                <Card.Text>
                    Some quick example text to build on the card title and make up the
                    bulk of the card's content.
                </Card.Text>
                </Card.Body>
                </Card>
             </Col>
        </Row>
  
  )
}

export default Highlights