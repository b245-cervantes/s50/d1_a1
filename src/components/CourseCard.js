
import {Col, Row, Card, Button} from 'react-bootstrap';
import React, { useEffect, useState, useContext } from 'react';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';


function CourseCard({courses}) {
    
    const {_id,name, description, price} = courses;

    //use the state hook for this component to be able to store its state
    // states are used to keep track of information related to individual components
       // const [gretter, setter] =useState (initialGetterValue);
       const [enrollees, setEnrollees] = useState(0);
       const [seats, setSeats] = useState(30);
       // add new stat that will declare whether the button disable or not
       const [isDisabled, setDisabled] =  useState(false)
       const {user} = useContext(UserContext);
        //initial value of enrollees state
       
       //if you want to change/ reassing the value of the state
           const enrolls =()=>{
              if(seats===0 && enrollees === 30) {
                       
                return true
               } else{
                 setEnrollees(enrollees + 1);
                 setSeats(seats - 1)
               }


            }
            
                  
                 function notReg (){
                  alert(`please login to enroll!`)
                }
          
          // Define a useEffect hook to have the "CourseCard" component do perform a certain taks
          //this will run automatically.
             //Syntax:
                // useEffect(sideEffect/function, [dependencies]);

                //sideEffect/function- it will run on the first load and will reload depending on the dependency array.

                useEffect(() => {
                   if(seats === 0) {
                    setDisabled(true)
                    alert("Congrats for the last enrollee for this courrse")
                   }
                }, [seats]);



              return ( 
              
            <Row className='mt-5'>
            <Col>
            <Card>
                        <Card.Body>
                        <Card.Title>{name}</Card.Title>
                        <Card.Subtitle>Description</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>
                        <Card.Subtitle>Price:</Card.Subtitle>
                        <Card.Text>{price}</Card.Text>

                        <Card.Subtitle>Enrollees:</Card.Subtitle>
                        <Card.Text>{enrollees}</Card.Text>

                        <Card.Subtitle>Available Sets:</Card.Subtitle>
                        <Card.Text>{seats}</Card.Text>

                        {
                          user ?
                          <Button as = {Link} to = {`/course/${_id}`} disabled ={isDisabled}>See more details</Button>
                          :
                          <Button onClick={notReg}  as = {Link} to = '/login' >Enroll</Button>
                        }
                        
                        </Card.Body>
                        </Card>
            </Col>
        </Row>
      
          )
        }

export default CourseCard

