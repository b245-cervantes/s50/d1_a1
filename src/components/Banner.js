import React from 'react';
//destructuring
import { Row, Col, Button } from 'react-bootstrap';

import {Link, NavLink} from 'react-router-dom';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';

function Banner() {
  const {user} = useContext(UserContext);

        function notReg (){
          alert(`please login to enroll!`)
        }
  return (
    
        <Row className='mt-5'>
            <Col className='text-center'>
                    <h1>Zuitt Coding Bootcamp</h1>
                    <p className='pt-1'>Opportunnities for everyone, everywhere.</p>
                    {
                      user ?
                      <Button className='pt-1' as = {Link} to = '/courses'>Enroll Now!</Button>
                      :
                      <Button className='pt-1' as = {Link} to = '/login' onClick={notReg}>Enroll Now!</Button>

                    }
                        
            </Col>
        </Row>
   
  )
}

export default Banner